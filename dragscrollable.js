/*
 * jQuery dragscrollable Plugin
 * version: 1.0 (25-Jun-2009)
 * Copyright (c) 2009 Miquel Herrera
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */
;(function ($) { // secure $ jQuery alias

    /**
    * Adds the ability to manage elements scroll by dragging
    * one or more of its descendant elements. Options parameter
    * allow to specifically select which inner elements will
    * respond to the drag events.
    * 
    * options properties:
    * ------------------------------------------------------------------------		
    *  dragSelector         | jquery selector to apply to each wrapped element 
    *                       | to find which will be the dragging elements. 
    *                       | Defaults to '>:first' which is the first child of 
    *                       | scrollable element
    * ------------------------------------------------------------------------		
    *  acceptPropagatedEvent| Will the dragging element accept propagated 
    *	                     | events? default is yes, a propagated mouse event 
    *	                     | on a inner element will be accepted and processed.
    *	                     | If set to false, only events originated on the
    *	                     | draggable elements will be processed.
    * ------------------------------------------------------------------------
    *  preventDefault       | Prevents the event to propagate further effectivey
    *                       | dissabling other default actions. Defaults to true
    * ------------------------------------------------------------------------
    *  
    *  usage examples:
    *
    *  To add the scroll by drag to the element id=viewport when dragging its 
    *  first child accepting any propagated events
    *	$('#viewport').dragscrollable(); 
    *
    *  To add the scroll by drag ability to any element div of class viewport
    *  when dragging its first descendant of class dragMe responding only to
    *  evcents originated on the '.dragMe' elements.
    *	$('div.viewport').dragscrollable({dragSelector:'.dragMe:first',
    *									  acceptPropagatedEvent: false});
    *
    *  Notice that some 'viewports' could be nested within others but events
    *  would not interfere as acceptPropagatedEvent is set to false.
    *		
    */
    $.fn.dragscrollable = function (options) {

        var settings = $.extend(
		{
		    dragSelector: '>:first',
		    acceptPropagatedEvent: true,
		    preventDefault: true,
		    avoidSelector: null
		}, options || {});


        var dragscroll = {
            mouseDownHandler: function(event) {
                var result = true;
                // mousedown, left click, check propagation
				var target;
				if (event.target)
				    target = event.target;
				else
				    if (event.srcElement)
				        target = event.srcElement;
                
				if (target.nodeType == 3) // defeat Safari bug
				   target = target.parentNode;

                var avoidElement = $(target).closest(event.data.avoidSelector).length > 0;
                var acceptEvent = (event.data.acceptPropagatedEvent &&  !avoidElement) || event.target == this;

                if (event.which != 1 || !acceptEvent)
                    result = false;
                else {
                    // Initial coordinates will be the last when dragging
                    event.data.lastCoord = { left: event.clientX, top: event.clientY };

                    $.event.add(document, "mouseup", dragscroll.mouseUpHandler, event.data);
                    $.event.add(document, "mousemove", dragscroll.mouseMoveHandler, event.data);
                    if (event.data.preventDefault) {
                        event.preventDefault();
                        result = false;
                    }    
                }
                return result;
            },
            mouseMoveHandler: function(event) { // User is dragging
                // How much did the mouse move?
                var delta = {
                    left: (event.clientX - event.data.lastCoord.left),
                    top: (event.clientY - event.data.lastCoord.top)
                };

                // Set the scroll position relative to what ever the scroll is now
                event.data.scrollable.scrollLeft(event.data.scrollable.scrollLeft() - delta.left);
                event.data.scrollable.scrollTop(event.data.scrollable.scrollTop() - delta.top);

                // Save where the cursor is
                event.data.lastCoord = { left: event.clientX, top: event.clientY };
                if (event.data.preventDefault) {
                    event.preventDefault();
                    return false;
                }

            },
            mouseUpHandler: function(event) { // Stop scrolling
                $.event.remove(document, "mousemove", dragscroll.mouseMoveHandler);
                $.event.remove(document, "mouseup", dragscroll.mouseUpHandler);
                if (event.data.preventDefault) {
                    event.preventDefault();
                    return false;
                }
            }
        };

        // set up the initial events
        this.each(function () {
            // closure object data for each scrollable element
            var $this = $(this);
            var data = {
                scrollable: $this,
                acceptPropagatedEvent: settings.acceptPropagatedEvent,
                preventDefault: settings.preventDefault,
                avoidSelector: settings.avoidSelector
            };
            
            // Set mouse initiating event on the desired descendant
            $(this)
                .find(settings.dragSelector)
                .bind('mousedown', data, dragscroll.mouseDownHandler);
        });
    };

	$.fn.minimap = function() {
        var originLeft;
        var originTop;
        var open;
        var miniMap;
        var miniMapCurrentView;
        var articlesPlaced;

        var el = this;

        //create map
        //iterate through articles and create 1/20th scale divs in map

        function setup() {
            //create map
            if ($('#mini-map').length == 0) {
                el.after('<div id="mini-map">');
                miniMap = $('#mini-map');
                miniMap.append('<div id="current-view">');
            }

            var childElements = el.children();

            childElements.each(function(i) {
                var articleCoords = $(this).position();

                var mapIconWidth = $(this).width() / 20;
                var mapIconHeight = $(this).height() / 20;
                var mapIconX = articleCoords.left/20;
                var mapIconY = articleCoords.top/20;
                
                var mapIcon = $('<div>');
                mapIcon.css({ 'width': mapIconWidth + 'px', 'height': mapIconHeight + 'px', 'left': mapIconX + 'px', 'top': mapIconY + 'px' });

                mapIcon.appendTo(miniMap);
            });
            el.find('.stick').each(function() {
                $(this).css('position', 'absolute');
            });

            mapView();
        }

        function mapView() {
            //set map size 1/20th of everything
            var miniMapWidth = ($(document).width() / 20) + 10;
            var miniMapHeight = ($(document).height() / 20) + 10;
            miniMap.css({ 'width': miniMapWidth + 'px', 'height': miniMapHeight + 'px' });

            //set current view
            miniMapCurrentView = $('#current-view');
            var miniMapCurrentViewWidth = ($(window).width() / 20) + 10;
            var miniMapCurrentViewHeight = ($(window).height() / 20 + 10);
            miniMapCurrentView.css({ 'width': miniMapCurrentViewWidth + 'px', 'height': miniMapCurrentViewHeight + 'px' });
        }
      
        $(window).resize(function() {
            el.find('.open').removeClass('open').removeClass('transition');
            open = false;

            //el.find('.stick').removeClass('stick').removeAttr('style');
            setTimeout(function() {
                mapView();
            }, 500);
        });

        $(window).scroll(function() {
            //reposition current view
            var miniMapCurrentViewX = $(window).scrollLeft() / 20;
            var miniMapCurrentViewY = $(window).scrollTop() / 20;

            //for iOS
            //miniMap.css({'left':$(window).scrollLeft()+20+'px', 'top':$(window).scrollTop()+20+'px'});
            miniMapCurrentView.css({ 'left': miniMapCurrentViewX + 'px', 'top': miniMapCurrentViewY + 'px' });
        });

        setup();
    };
})(jQuery); // confine scope